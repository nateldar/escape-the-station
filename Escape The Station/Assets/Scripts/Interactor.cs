﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interactor : MonoBehaviour
{
    public Transform aimTransform;
    public float interactionDistance =1.5f;
    public Text TxtInfos;
    public TakeLetPipe pipeManager;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (!pipeManager.GetPrend()) {
            TxtInfos.text = "";
            if (Physics.Raycast(aimTransform.position, aimTransform.forward, out hit,interactionDistance)) {

                WriteTxtMessage(hit);
                if (Input.GetKeyDown(KeyCode.E)) {
                    Interact(hit);
                }
            }
        } else {
            TxtInfos.text = "E pour LÂCHER";
            if (Physics.Raycast(aimTransform.position, aimTransform.forward, out hit,interactionDistance)) {
                WriteTxtMessagePrend(hit);
                if (Input.GetKeyDown(KeyCode.E)) {
                    Interact(hit);
                }
            } else {
                if (Input.GetKeyDown(KeyCode.E)) {
                    pipeManager.Lacher();
                }
            }
        }
    }

    void WriteTxtMessage(RaycastHit hit) {
        GameObject obj = hit.collider.gameObject;
        if (obj.tag == "Pushable" && obj.GetComponent<PinButton>()!=null) {
            TxtInfos.text = "E pour TAPER";
        } else if (obj.tag == "Door" && obj.GetComponent<DoorOpenning>()!=null){
            TxtInfos.text = "E pour OUVRIR/FERMER";
        } else if (obj.tag == "Pipe") {
            TxtInfos.text = "E pour RAMASSER";
        } else if (obj.tag == "PipeLet" && obj.GetComponent<ValidationPipe>().GetContenuActuel() != null) {
            TxtInfos.text = "E pour RAMASSER";
        }
    }

    void WriteTxtMessagePrend(RaycastHit hit) {
        GameObject obj = hit.collider.gameObject;
        if (obj.tag == "PipeLet") {
            if (obj.GetComponent<ValidationPipe>().GetContenuActuel() == null) {
                TxtInfos.text = "E pour DÉPOSER";
            } else {
                TxtInfos.text = "";
            }
        } else if (obj.tag == "Pipe" && obj != pipeManager.GetGoName()) {
            TxtInfos.text = "";
        }
    }

    void Interact(RaycastHit hit) {
        GameObject obj = hit.collider.gameObject;
        if (obj.tag == "Pushable" && obj.GetComponent<PinButton>()!=null) {
          obj.GetComponent<PinButton>().push();
        } else if (obj.tag == "Door" && obj.GetComponent<DoorOpenning>()!=null){
            obj.GetComponent<DoorOpenning>().ToggleDoor();
        } else if (obj.tag == "Pipe" && !pipeManager.GetPrend()) {
            pipeManager.Take(obj);
        } else if (obj.tag == "Pipe" && pipeManager.GetPrend() && obj == pipeManager.GetGoName()) {
            pipeManager.Lacher();
        } else if (obj.tag == "PipeLet") {
            if (obj.GetComponent<ValidationPipe>().GetContenuActuel() != null && !pipeManager.GetPrend()) {
                pipeManager.Take(obj.GetComponent<ValidationPipe>().GetContenuActuel());
            } else if (obj.GetComponent<ValidationPipe>().GetContenuActuel() == null && pipeManager.GetPrend()) {
                pipeManager.Deposer(obj);
            }
        } else {
          //Debug.Log("unknown tag:" + obj.tag);
        }
    }
}
