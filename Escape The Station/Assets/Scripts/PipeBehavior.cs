﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeBehavior : MonoBehaviour
{

    public string codeValidation = "";
    public GameObject salle;
    private GameObject conteneurActuel = null;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public string GetCodeValidation() {
        return codeValidation;
    }

    public GameObject GetSalle() {
        return salle;
    }

    public GameObject GetConteneurActuel() {
        return conteneurActuel;
    }

    public void SetConteneurActuel(GameObject cont) {
        conteneurActuel = cont;
    }
}
