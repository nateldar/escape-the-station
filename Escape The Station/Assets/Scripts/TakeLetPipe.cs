﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TakeLetPipe : MonoBehaviour
{
    private bool prend = false;
    private GameObject goName;
    public GameObject holder;
    public float keepDistance=0.05f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
      if (prend && Vector3.Distance(goName.transform.position, holder.transform.position)> keepDistance ) {
        goName.GetComponent<Rigidbody>().velocity = holder.transform.position-goName.transform.position; // keep the grabbed pipe at reasonable distance
      }
    }

    public void Take(GameObject obj) {
        goName = obj;
        PipeBehavior pb = goName.GetComponent<PipeBehavior>();
        if (pb.GetConteneurActuel() != null) {
            pb.GetConteneurActuel().GetComponent<ValidationPipe>().EnlevePipe();
            pb.SetConteneurActuel(null);
        }
        if (!prend) {
            goName.GetComponent<Rigidbody>().isKinematic = false;
            goName.GetComponent<Rigidbody>().velocity = Vector3.zero;
            goName.transform.position = holder.transform.position;
            goName.gameObject.transform.parent = holder.transform;
            prend = true;
        }
    }

    public void Lacher() {
        if (prend) {
            goName.GetComponent<Rigidbody>().isKinematic = false;
            PipeBehavior pb = goName.GetComponent<PipeBehavior>();
            goName.gameObject.transform.parent = pb.GetSalle().transform;
            prend = false;
        }
    }

    public void Deposer(GameObject conteneur) {
        goName.GetComponent<Rigidbody>().isKinematic = true;
        goName.GetComponent<Rigidbody>().velocity = Vector3.zero;
        goName.transform.position = conteneur.transform.position;
        goName.transform.rotation = conteneur.transform.rotation;
        goName.gameObject.transform.parent = conteneur.transform;
        goName.GetComponent<PipeBehavior>().SetConteneurActuel(conteneur);
        // on donne au conteneur le script du pipe placé
        ValidationPipe vp = conteneur.GetComponent<ValidationPipe>();
        vp.ModifValide(goName.GetComponent<PipeBehavior>());
        vp.SetContenuActuel(goName);
        prend = false;
    }

    public bool GetPrend() {
        return prend;
    }

    public GameObject GetGoName() {
        return goName;
    }
}
