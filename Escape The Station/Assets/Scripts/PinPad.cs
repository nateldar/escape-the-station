﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void ResolveDelegate();

public class PinPad : MonoBehaviour
{
    public string solution="0000";
    private string currentEntry="";
    public DoorOpenning door;
    public List<GameObject> leds;
    public Material ledON;
    public Material ledOFF;
    private int nextLedIndex;
    // Start is called before the first frame update
    void Start()
    {
      nextLedIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddNumber(int value) {
      currentEntry = currentEntry + value;
      GetComponent<AudioSource>().Play();
      if (nextLedIndex < leds.Count) {
        leds[nextLedIndex].GetComponent<MeshRenderer>().material = ledON;
        nextLedIndex= nextLedIndex+1;
      }

      if(solution == currentEntry) {
        door.SetLockState(false);
      } else {
        if (solution.Length <= currentEntry.Length) {
          currentEntry = "";
          foreach (GameObject led in leds) {
            led.GetComponent<MeshRenderer>().material = ledOFF;
            nextLedIndex=0;
          }
        }
      }
    }
}
