﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpenning : MonoBehaviour
{
    public bool opened = false;
    public bool locked = true;
    // Start is called before the first frame update
    void Start()
    {

      if (locked) {
        opened = false;
      }
      GetComponent<Animator>().SetBool("Opened", opened);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ToggleDoor() {
      if (!locked) {
        GetComponent<AudioSource>().Play();
        opened = !opened;
        GetComponent<Animator>().SetBool("Opened", opened);
      }
    }

    public void SetLockState(bool isLocked) {
      locked = isLocked;
    }
}
