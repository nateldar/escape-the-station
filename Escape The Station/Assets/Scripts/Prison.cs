﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prison : MonoBehaviour
{
    public DoorOpenning door;
    public Collider trig;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col) {
        GameObject obj = col.gameObject;
        if (obj.tag == "Player") {
            door.ToggleDoor();
            door.SetLockState(true);
            GetComponent<AudioSource>().Play();
            trig.enabled = false;
        }
    }
}
