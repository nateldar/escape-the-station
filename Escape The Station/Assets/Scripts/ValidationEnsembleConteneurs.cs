﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValidationEnsembleConteneurs : MonoBehaviour
{
    public GameObject[] ensemble;
    public DoorOpenning[] doors;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private bool porteOuvrable() {
        bool ouvrable = true;
        foreach(GameObject cont in ensemble)
        {
           ouvrable = ouvrable && cont.GetComponent<ValidationPipe>().GetValide();
        }
        return ouvrable;
    }

    public void ouvrirPorteSiPossible() {
        if (porteOuvrable()) {
            foreach(DoorOpenning door in doors)
            {
                door.SetLockState(false);
                door.ToggleDoor();
            }
        }
    }

    public DoorOpenning[] GetDoors() {
        return doors;
    }
}
