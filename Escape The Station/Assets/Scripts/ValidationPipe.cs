﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValidationPipe : MonoBehaviour
{
    private GameObject contenuActuel = null;
    public string codeBon;
    public bool valide = false;
    public GameObject ensemble;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ModifValide(PipeBehavior pB) {
        string cv = pB.GetCodeValidation();
        valide = (cv == codeBon);
        ensemble.GetComponent<ValidationEnsembleConteneurs>().ouvrirPorteSiPossible();
    }

    public bool GetValide() {
        return valide;
    }

    public GameObject GetContenuActuel() {
        return contenuActuel;
    }

    public void SetContenuActuel(GameObject cont) {
        contenuActuel = cont;
    }

    public void EnlevePipe() {
        valide = false;
        contenuActuel = null;

        foreach(DoorOpenning door in ensemble.GetComponent<ValidationEnsembleConteneurs>().GetDoors())
        {
            door.ToggleDoor();
            door.SetLockState(true);
        }
    }
}
