﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPlayerController : MonoBehaviour
{
    public float moveSpeed = 5.0f;
    public float horizontalRotationSpeed =1.0f;
    public float verticalRotationSpeed =1.0f;
    private Transform playerTransform;
    private CharacterController characterController;
    // Start is called before the first frame update
    void Start()
    {
      characterController = GetComponent<CharacterController>();
      //playerTransform = GetComponent<Transform>();
      playerTransform = characterController.transform;

      // si menu de pause, changer de place
      Cursor.lockState = CursorLockMode.Locked;
      Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
      float horizontalInput = Input.GetAxis("Horizontal");
      float verticalInput = Input.GetAxis("Vertical");
      float depthInput = Input.GetAxis("Depth"); // this is a custom axis made for the project
      Vector3 movement = Vector3.Normalize(playerTransform.right * horizontalInput + playerTransform.forward * verticalInput + playerTransform.up*depthInput);

      characterController.Move(movement * moveSpeed * Time.deltaTime);

      float mouseX = Input.GetAxis("Mouse X") * horizontalRotationSpeed;
      float mouseY = Input.GetAxis("Mouse Y") * verticalRotationSpeed;
      playerTransform.Rotate(-mouseY,mouseX,0.0f);

    }

}
